package generatedStateMachine;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

class StatemachineTests {

	App app;

	@BeforeEach
	public void initialize() {
		app = new App();
	}

	@Test
	@DisplayName("Throws an illegal state exception on request to process an Unknown State")
	void illegalStateThrows() {

		App app = new App();

		// The statement below will succeed
		app.process("AABC");

		// The statement below will fail because
		// there is no transition from A to C
		// in the state machine
		Executable executable = () -> app.process("ACB");
		assertThrows(IllegalStateException.class, executable);

	}

}
